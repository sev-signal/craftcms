#!/usr/bin/env bash

DIR=/shore_site
# Commands to provision the site

docker exec -it craft-demo bash -l -c "( cd $DIR ; export COMPOSER_HOME=$DIR/.shore/.composer ; sh ./scripts/provision.sh $@ )"
