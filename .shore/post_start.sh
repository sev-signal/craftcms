#!/usr/bin/env bash

# Commands to run after the containers start

docker exec -i craft-demo bash -c ' \
INST=$(composer global --working-dir=/shore_site/current/.shore/.composer show 2> /dev/null | grep prestissimo) ; \
if [ -z "$INST" ]; then \
  composer global --no-interaction require hirak/prestissimo ; \
fi'
