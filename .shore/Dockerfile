FROM centos:7

RUN yum install -y epel-release && \
    rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm && \
    yum update -y && \
    yum install -y \
    which wget git unzip curl vim gcc make httpd \
    php72w \
    php72w-devel \
    php72w-opcache \
    php72w-bcmath \
    php72w-gd \
    php72w-intl \
    php72w-json \
    php72w-pdo \
    php72w-mbstring \
    php72w-mysqlnd \
    php72w-pecl-imagick \
    php72w-xml \
    php72w-soap \
    php72w-pecl-memcached \
    zlib-devel \
    mariadb \
    ssmtp \
    libmemcached-devel \
    libmcrypt-devel && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN yum groupinstall -y "Development Tools"

RUN curl -sL https://rpm.nodesource.com/setup_8.x | bash - && yum -y install nodejs

RUN usermod -u 1000 apache && \
    npm config set cache ../.cache/npm --global && \
    npm install -g --unsafe-perm node-sass && \
    npm install -g bower  && \
    npm install -g grunt

WORKDIR /shore_site/current

RUN pecl install xdebug
RUN pecl install mcrypt-1.0.2

RUN rm -f /etc/php.d/xdebug.ini

COPY ./config/xdebug.ini /etc/php.d/xdebug.ini.disabled

COPY ./config/php.ini /etc/php.ini

COPY ./config/shore.conf /etc/httpd/conf.d/shore.conf

RUN rm /etc/httpd/conf.d/welcome.conf \
    && sed -i "s/AllowOverride \None/AllowOverride\ All/g" /etc/httpd/conf/httpd.conf

RUN sed -i "s/mailhub=mail$/mailhub=mail\:1025/g" /etc/ssmtp/ssmtp.conf

RUN ln -sf /dev/stdout /var/log/httpd/access_log && \
    ln -sf /dev/stderr /var/log/httpd/error_log

CMD httpd -DFOREGROUND
